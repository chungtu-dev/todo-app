export interface ITask{
    desc: string,
    done: boolean
}